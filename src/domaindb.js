/* jslint node:true */

'use strict';

exports = module.exports = {
    add: add,
    get: get,
    getAll: getAll,
    update: update,
    del: del,
    clear: clear
};

var assert = require('assert'),
    BoxError = require('./boxerror.js'),
    database = require('./database.js'),
    safe = require('safetydance');

var DOMAINS_FIELDS = [ 'domain', 'zoneName', 'provider', 'configJson', 'tlsConfigJson' ].join(',');

function postProcess(data) {
    data.config = safe.JSON.parse(data.configJson);
    data.tlsConfig = safe.JSON.parse(data.tlsConfigJson);
    delete data.configJson;
    delete data.tlsConfigJson;

    return data;
}

function get(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    database.query(`SELECT ${DOMAINS_FIELDS} FROM domains WHERE domain=?`, [ domain ], function (error, result) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (result.length === 0) return callback(new BoxError(BoxError.NOT_FOUND, 'Domain not found'));

        postProcess(result[0]);

        callback(null, result[0]);
    });
}

function getAll(callback) {
    database.query(`SELECT ${DOMAINS_FIELDS} FROM domains ORDER BY domain`, function (error, results) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        results.forEach(postProcess);

        callback(null, results);
    });
}

function add(name, data, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof data, 'object');
    assert.strictEqual(typeof data.zoneName, 'string');
    assert.strictEqual(typeof data.provider, 'string');
    assert.strictEqual(typeof data.config, 'object');
    assert.strictEqual(typeof data.tlsConfig, 'object');
    assert.strictEqual(typeof callback, 'function');

    let queries = [
        { query: 'INSERT INTO domains (domain, zoneName, provider, configJson, tlsConfigJson) VALUES (?, ?, ?, ?, ?)', args: [ name, data.zoneName, data.provider, JSON.stringify(data.config), JSON.stringify(data.tlsConfig) ] },
        { query: 'INSERT INTO mail (domain, dkimSelector) VALUES (?, ?)', args: [ name, data.dkimSelector || 'cloudron' ] },
    ];

    database.transaction(queries, function (error) {
        if (error && error.code === 'ER_DUP_ENTRY') return callback(new BoxError(BoxError.ALREADY_EXISTS, 'Domain already exists'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function update(name, domain, callback) {
    assert.strictEqual(typeof name, 'string');
    assert.strictEqual(typeof domain, 'object');
    assert.strictEqual(typeof callback, 'function');

    var args = [ ], fields = [ ];
    for (var k in domain) {
        if (k === 'config') {
            fields.push('configJson = ?');
            args.push(JSON.stringify(domain[k]));
        } else if (k === 'tlsConfig') {
            fields.push('tlsConfigJson = ?');
            args.push(JSON.stringify(domain[k]));
        } else {
            fields.push(k + ' = ?');
            args.push(domain[k]);
        }
    }
    args.push(name);

    database.query('UPDATE domains SET ' + fields.join(', ') + ' WHERE domain=?', args, function (error) {
        if (error && error.reason === BoxError.NOT_FOUND) return callback(new BoxError(BoxError.NOT_FOUND, 'Domain not found'));
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(null);
    });
}

function del(domain, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof callback, 'function');

    let queries = [
        { query: 'DELETE FROM mail WHERE domain = ?', args: [ domain ] },
        { query: 'DELETE FROM domains WHERE domain = ?', args: [ domain ] },
    ];

    database.transaction(queries, function (error, results) {
        if (error && error.code === 'ER_ROW_IS_REFERENCED_2') {
            if (error.message.indexOf('apps_mailDomain_constraint') !== -1) return callback(new BoxError(BoxError.CONFLICT, 'Domain is in use by an app or the mailbox of an app. Check the domains of apps and the Email section of each app.'));
            if (error.message.indexOf('subdomains') !== -1) return callback(new BoxError(BoxError.CONFLICT, 'Domain is in use by one or more app(s).'));
            if (error.message.indexOf('mail') !== -1) return callback(new BoxError(BoxError.CONFLICT, 'Domain is in use by one or more mailboxes. Delete them first in the Email view.'));

            return callback(new BoxError(BoxError.CONFLICT, error.message));
        }

        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));
        if (results[1].affectedRows !== 1) return callback(new BoxError(BoxError.NOT_FOUND, 'Domain not found'));

        callback(null);
    });
}

function clear(callback) {
    database.query('DELETE FROM domains', function (error) {
        if (error) return callback(new BoxError(BoxError.DATABASE_ERROR, error));

        callback(error);
    });
}
