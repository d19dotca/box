'use strict';

// IMPORTANT: These patterns are together because they spin tasks which acquire a lock
// If the patterns overlap all the time, then the task may not ever get a chance to run!
// If you change this change dashboard patterns in settings.html
const DEFAULT_CLEANUP_BACKUPS_PATTERN = '00 30 1,3,5,23 * * *',
    DEFAULT_BOX_ENSURE_BACKUP_PATTERN_LT_6HOURS = '00 45 1,7,13,19 * * *',
    DEFAULT_BOX_ENSURE_BACKUP_PATTERN_GT_6HOURS =  '00 45 1,3,5,23 * * *',
    DEFAULT_BOX_AUTOUPDATE_PATTERN = '00 00 1,3,5,23 * * *',
    DEFAULT_APP_AUTOUPDATE_PATTERN = '00 15 1,3,5,23 * * *';

exports = module.exports = {
    startJobs,

    stopJobs,

    handleSettingsChanged,

    DEFAULT_BOX_AUTOUPDATE_PATTERN,
    DEFAULT_APP_AUTOUPDATE_PATTERN
};

var appHealthMonitor = require('./apphealthmonitor.js'),
    apps = require('./apps.js'),
    assert = require('assert'),
    async = require('async'),
    auditSource = require('./auditsource.js'),
    backups = require('./backups.js'),
    cloudron = require('./cloudron.js'),
    constants = require('./constants.js'),
    CronJob = require('cron').CronJob,
    debug = require('debug')('box:cron'),
    dyndns = require('./dyndns.js'),
    eventlog = require('./eventlog.js'),
    janitor = require('./janitor.js'),
    scheduler = require('./scheduler.js'),
    settings = require('./settings.js'),
    system = require('./system.js'),
    updater = require('./updater.js'),
    updateChecker = require('./updatechecker.js');

var gJobs = {
    appAutoUpdater: null,
    boxAutoUpdater: null,
    appUpdateChecker: null,
    backup: null,
    boxUpdateChecker: null,
    systemChecks: null,
    diskSpaceChecker: null,
    certificateRenew: null,
    cleanupBackups: null,
    cleanupEventlog: null,
    cleanupTokens: null,
    dockerVolumeCleaner: null,
    dynamicDns: null,
    schedulerSync: null,
    appHealthMonitor: null
};

var NOOP_CALLBACK = function (error) { if (error) debug(error); };

// cron format
// Seconds: 0-59
// Minutes: 0-59
// Hours: 0-23
// Day of Month: 1-31
// Months: 0-11
// Day of Week: 0-6

function startJobs(callback) {
    assert.strictEqual(typeof callback, 'function');

    const randomMinute = Math.floor(60*Math.random());
    gJobs.systemChecks = new CronJob({
        cronTime: '00 30 * * * *', // every 30 minutes. if you change this interval, change the notification messages with correct duration
        onTick: () => cloudron.runSystemChecks(NOOP_CALLBACK),
        start: true
    });

    gJobs.diskSpaceChecker = new CronJob({
        cronTime: '00 30 * * * *', // every 30 minutes. if you change this interval, change the notification messages with correct duration
        onTick: () => system.checkDiskSpace(NOOP_CALLBACK),
        start: true
    });

    gJobs.boxUpdateCheckerJob = new CronJob({
        cronTime: '00 ' + randomMinute + ' 1,3,5,21,23 * * *', // 5 times
        onTick: () => updateChecker.checkBoxUpdates({ automatic: true }, NOOP_CALLBACK),
        start: true
    });

    gJobs.appUpdateChecker = new CronJob({
        cronTime: '00 ' + randomMinute + ' 2,4,6,20,22 * * *', // 5 times
        onTick: () => updateChecker.checkAppUpdates({ automatic: true }, NOOP_CALLBACK),
        start: true
    });

    gJobs.cleanupTokens = new CronJob({
        cronTime: '00 */30 * * * *', // every 30 minutes
        onTick: janitor.cleanupTokens,
        start: true
    });

    gJobs.cleanupBackups = new CronJob({
        cronTime: DEFAULT_CLEANUP_BACKUPS_PATTERN,
        onTick: backups.startCleanupTask.bind(null, auditSource.CRON, NOOP_CALLBACK),
        start: true
    });

    gJobs.cleanupEventlog = new CronJob({
        cronTime: '00 */30 * * * *', // every 30 minutes
        onTick: eventlog.cleanup,
        start: true
    });

    gJobs.dockerVolumeCleaner = new CronJob({
        cronTime: '00 00 */12 * * *', // every 12 hours
        onTick: janitor.cleanupDockerVolumes,
        start: true
    });

    gJobs.schedulerSync = new CronJob({
        cronTime: constants.TEST ? '*/10 * * * * *' : '00 */1 * * * *', // every minute
        onTick: scheduler.sync,
        start: true
    });

    gJobs.certificateRenew = new CronJob({
        cronTime: '00 00 */12 * * *', // every 12 hours
        onTick: cloudron.renewCerts.bind(null, {}, auditSource.CRON, NOOP_CALLBACK),
        start: true
    });

    gJobs.appHealthMonitor = new CronJob({
        cronTime: '*/10 * * * * *', // every 10 seconds
        onTick: appHealthMonitor.run.bind(null, 10, NOOP_CALLBACK),
        start: true
    });

    settings.getAll(function (error, allSettings) {
        if (error) return callback(error);

        const tz = allSettings[settings.TIME_ZONE_KEY];
        backupConfigChanged(allSettings[settings.BACKUP_CONFIG_KEY], tz);
        appAutoupdatePatternChanged(allSettings[settings.APP_AUTOUPDATE_PATTERN_KEY], tz);
        boxAutoupdatePatternChanged(allSettings[settings.BOX_AUTOUPDATE_PATTERN_KEY], tz);
        dynamicDnsChanged(allSettings[settings.DYNAMIC_DNS_KEY]);

        callback();
    });
}

// eslint-disable-next-line no-unused-vars
function handleSettingsChanged(key, value) {
    assert.strictEqual(typeof key, 'string');
    // value is a variant

    switch (key) {
    case settings.TIME_ZONE_KEY:
    case settings.BACKUP_CONFIG_KEY:
    case settings.APP_AUTOUPDATE_PATTERN_KEY:
    case settings.BOX_AUTOUPDATE_PATTERN_KEY:
    case settings.DYNAMIC_DNS_KEY:
        debug('handleSettingsChanged: recreating all jobs');
        async.series([
            stopJobs,
            startJobs
        ], NOOP_CALLBACK);
        break;
    default:
        break;
    }
}

function backupConfigChanged(value, tz) {
    assert.strictEqual(typeof value, 'object');
    assert.strictEqual(typeof tz, 'string');

    debug(`backupConfigChanged: interval ${value.intervalSecs} (${tz})`);

    if (gJobs.backup) gJobs.backup.stop();
    let pattern;
    if (value.intervalSecs <= 6 * 60 * 60) {
        pattern = DEFAULT_BOX_ENSURE_BACKUP_PATTERN_LT_6HOURS; // no option but to backup in the middle of the day
    } else {
        pattern = DEFAULT_BOX_ENSURE_BACKUP_PATTERN_GT_6HOURS; // avoid middle of the day backups. it's 45 to not overlap auto-updates
    }

    gJobs.backup = new CronJob({
        cronTime: pattern,
        onTick: backups.ensureBackup.bind(null, auditSource.CRON, NOOP_CALLBACK),
        start: true,
        timeZone: tz
    });
}

function boxAutoupdatePatternChanged(pattern, tz) {
    assert.strictEqual(typeof pattern, 'string');
    assert.strictEqual(typeof tz, 'string');

    debug(`boxAutoupdatePatternChanged: pattern - ${pattern} (${tz})`);

    if (gJobs.boxAutoUpdater) gJobs.boxAutoUpdater.stop();

    if (pattern === constants.AUTOUPDATE_PATTERN_NEVER) return;

    gJobs.boxAutoUpdater = new CronJob({
        cronTime: pattern,
        onTick: function() {
            var updateInfo = updateChecker.getUpdateInfo();
            if (updateInfo.box) {
                debug('Starting autoupdate to %j', updateInfo.box);
                updater.updateToLatest({ skipBackup: false }, auditSource.CRON, NOOP_CALLBACK);
            } else {
                debug('No box auto updates available');
            }
        },
        start: true,
        timeZone: tz
    });
}

function appAutoupdatePatternChanged(pattern, tz) {
    assert.strictEqual(typeof pattern, 'string');
    assert.strictEqual(typeof tz, 'string');

    debug(`appAutoupdatePatternChanged: pattern ${pattern} (${tz})`);

    if (gJobs.appAutoUpdater) gJobs.appAutoUpdater.stop();

    if (pattern === constants.AUTOUPDATE_PATTERN_NEVER) return;

    gJobs.appAutoUpdater = new CronJob({
        cronTime: pattern,
        onTick: function() {
            var updateInfo = updateChecker.getUpdateInfo();
            if (updateInfo.apps) {
                debug('Starting app update to %j', updateInfo.apps);
                apps.autoupdateApps(updateInfo.apps, auditSource.CRON, NOOP_CALLBACK);
            } else {
                debug('No app auto updates available');
            }
        },
        start: true,
        timeZone: tz
    });
}

function dynamicDnsChanged(enabled) {
    assert.strictEqual(typeof enabled, 'boolean');

    debug('Dynamic DNS setting changed to %s', enabled);

    if (enabled) {
        gJobs.dynamicDns = new CronJob({
            cronTime: '5 * * * * *',    // we only update the records if the ip has changed.
            onTick: dyndns.sync.bind(null, auditSource.CRON, NOOP_CALLBACK),
            start: true
        });
    } else {
        if (gJobs.dynamicDns) gJobs.dynamicDns.stop();
        gJobs.dynamicDns = null;
    }
}

function stopJobs(callback) {
    assert.strictEqual(typeof callback, 'function');

    for (var job in gJobs) {
        if (!gJobs[job]) continue;
        gJobs[job].stop();
        gJobs[job] = null;
    }

    callback();
}
