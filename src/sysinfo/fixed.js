'use strict';

exports = module.exports = {
    getServerIp,
    testConfig
};

var assert = require('assert'),
    BoxError = require('../boxerror.js'),
    validator = require('validator');

function getServerIp(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    callback(null, config.ip);
}

function testConfig(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (typeof config.ip !== 'string') return callback(new BoxError(BoxError.BAD_FIELD, 'ip must be a string'));
    if (!validator.isIP(config.ip, 4)) return callback(new BoxError(BoxError.BAD_FIELD, 'ip is not a valid ipv4'));

    callback(null);
}
