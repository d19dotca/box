/* jslint node:true */
/* global it:false */
/* global describe:false */

'use strict';

var accesscontrol = require('../accesscontrol.js'),
    expect = require('expect.js'),
    HttpError = require('connect-lastmile').HttpError;

describe('access control middleware', function () {
    describe('passwordAuth', function () {
        // TBD
    });

    describe('tokenAuth', function () {
        // TBD
    });

    describe('authorize', function () {
        // TBD
    });

    describe('websocketAuth', function () {
        // TBD
    });
});
