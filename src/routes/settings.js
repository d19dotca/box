'use strict';

exports = module.exports = {
    set,
    get,

    // owner only settings
    setBackupConfig
};

var assert = require('assert'),
    backups = require('../backups.js'),
    BoxError = require('../boxerror.js'),
    docker = require('../docker.js'),
    externalLdap = require('../externalldap.js'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    settings = require('../settings.js');

function getAppAutoupdatePattern(req, res, next) {
    settings.getAppAutoupdatePattern(function (error, pattern) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { pattern: pattern }));
    });
}

function setAppAutoupdatePattern(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.pattern !== 'string') return next(new HttpError(400, 'pattern is required'));

    settings.setAppAutoupdatePattern(req.body.pattern, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getBoxAutoupdatePattern(req, res, next) {
    settings.getBoxAutoupdatePattern(function (error, pattern) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { pattern: pattern }));
    });
}

function setBoxAutoupdatePattern(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.pattern !== 'string') return next(new HttpError(400, 'pattern is required'));

    settings.setBoxAutoupdatePattern(req.body.pattern, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getTimeZone(req, res, next) {
    settings.getTimeZone(function (error, tz) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { timeZone: tz }));
    });
}

function setTimeZone(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.timeZone !== 'string') return next(new HttpError(400, 'timeZone is required'));

    settings.setTimeZone(req.body.timeZone, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getSupportConfig(req, res, next) {
    settings.getSupportConfig(function (error, supportConfig) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, supportConfig));
    });
}

function getBackupConfig(req, res, next) {
    settings.getBackupConfig(function (error, backupConfig) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, backups.removePrivateFields(backupConfig)));
    });
}

function setBackupConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));
    if (typeof req.body.intervalSecs !== 'number') return next(new HttpError(400, 'intervalSecs is required'));
    if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password must be a string'));
    if ('syncConcurrency' in req.body) {
        if (typeof req.body.syncConcurrency !== 'number') return next(new HttpError(400, 'syncConcurrency must be a positive integer'));
        if (req.body.syncConcurrency < 1) return next(new HttpError(400, 'syncConcurrency must be a positive integer'));
    }
    if (typeof req.body.format !== 'string') return next(new HttpError(400, 'format must be a string'));
    if ('acceptSelfSignedCerts' in req.body && typeof req.body.acceptSelfSignedCerts !== 'boolean') return next(new HttpError(400, 'format must be a boolean'));

    if (!req.body.retentionPolicy || typeof req.body.retentionPolicy !== 'object') return next(new HttpError(400, 'retentionPolicy is required'));

    // testing the backup using put/del takes a bit of time at times
    req.clearTimeout();

    settings.setBackupConfig(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getPlatformConfig(req, res, next) {
    settings.getPlatformConfig(function (error, config) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, config));
    });
}

function setPlatformConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    for (let addon of [ 'mysql', 'postgresql', 'mail', 'mongodb' ]) {
        if (!(addon in req.body)) continue;
        if (typeof req.body[addon] !== 'object') return next(new HttpError(400, 'addon config must be an object'));

        if (typeof req.body[addon].memory !== 'number') return next(new HttpError(400, 'memory must be a number'));
        if (typeof req.body[addon].memorySwap !== 'number') return next(new HttpError(400, 'memorySwap must be a number'));
    }

    settings.setPlatformConfig(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getExternalLdapConfig(req, res, next) {
    settings.getExternalLdapConfig(function (error, config) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, externalLdap.removePrivateFields(config)));
    });
}

function setExternalLdapConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider must be a string'));
    if ('url' in req.body && typeof req.body.url !== 'string') return next(new HttpError(400, 'url must be a string'));
    if ('baseDn' in req.body && typeof req.body.baseDn !== 'string') return next(new HttpError(400, 'baseDn must be a string'));
    if ('usernameField' in req.body && typeof req.body.usernameField !== 'string') return next(new HttpError(400, 'usernameField must be a string'));
    if ('filter' in req.body && typeof req.body.filter !== 'string') return next(new HttpError(400, 'filter must be a string'));
    if ('groupBaseDn' in req.body && typeof req.body.groupBaseDn !== 'string') return next(new HttpError(400, 'groupBaseDn must be a string'));
    if ('bindDn' in req.body && typeof req.body.bindDn !== 'string') return next(new HttpError(400, 'bindDn must be a non empty string'));
    if ('bindPassword' in req.body && typeof req.body.bindPassword !== 'string') return next(new HttpError(400, 'bindPassword must be a string'));

    settings.setExternalLdapConfig(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getDynamicDnsConfig(req, res, next) {
    settings.getDynamicDnsConfig(function (error, enabled) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { enabled: enabled }));
    });
}

function setDynamicDnsConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled boolean is required'));

    settings.setDynamicDnsConfig(req.body.enabled, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getUnstableAppsConfig(req, res, next) {
    settings.getUnstableAppsConfig(function (error, enabled) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, { enabled: enabled }));
    });
}

function setUnstableAppsConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.enabled !== 'boolean') return next(new HttpError(400, 'enabled boolean is required'));

    settings.setUnstableAppsConfig(req.body.enabled, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function getRegistryConfig(req, res, next) {
    settings.getRegistryConfig(function (error, registryConfig) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, docker.removePrivateFields(registryConfig)));
    });
}

function setRegistryConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (typeof req.body.serverAddress !== 'string') return next(new HttpError(400, 'serverAddress is required'));
    if ('username' in req.body && typeof req.body.username !== 'string') return next(new HttpError(400, 'username is required'));
    if ('email' in req.body && typeof req.body.email !== 'string') return next(new HttpError(400, 'email is required'));
    if ('password' in req.body && typeof req.body.password !== 'string') return next(new HttpError(400, 'password is required'));

    settings.setRegistryConfig(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200));
    });
}

function getSysinfoConfig(req, res, next) {
    settings.getSysinfoConfig(function (error, sysinfoConfig) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, sysinfoConfig));
    });
}

function setSysinfoConfig(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    if (!req.body.provider || typeof req.body.provider !== 'string') return next(new HttpError(400, 'provider is required'));

    settings.setSysinfoConfig(req.body, function (error) {
        if (error) return next(BoxError.toHttpError(error));

        next(new HttpSuccess(200, {}));
    });
}

function get(req, res, next) {
    assert.strictEqual(typeof req.params.setting, 'string');

    switch (req.params.setting) {
    case settings.DYNAMIC_DNS_KEY: return getDynamicDnsConfig(req, res, next);
    case settings.BACKUP_CONFIG_KEY: return getBackupConfig(req, res, next);
    case settings.PLATFORM_CONFIG_KEY: return getPlatformConfig(req, res, next);
    case settings.EXTERNAL_LDAP_KEY: return getExternalLdapConfig(req, res, next);
    case settings.UNSTABLE_APPS_KEY: return getUnstableAppsConfig(req, res, next);
    case settings.REGISTRY_CONFIG_KEY: return getRegistryConfig(req, res, next);
    case settings.SYSINFO_CONFIG_KEY: return getSysinfoConfig(req, res, next);

    case settings.APP_AUTOUPDATE_PATTERN_KEY: return getAppAutoupdatePattern(req, res, next);
    case settings.BOX_AUTOUPDATE_PATTERN_KEY: return getBoxAutoupdatePattern(req, res, next);
    case settings.TIME_ZONE_KEY: return getTimeZone(req, res, next);

    case settings.SUPPORT_CONFIG_KEY: return getSupportConfig(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}

function set(req, res, next) {
    assert.strictEqual(typeof req.body, 'object');

    switch (req.params.setting) {
    case settings.DYNAMIC_DNS_KEY: return setDynamicDnsConfig(req, res, next);
    case settings.PLATFORM_CONFIG_KEY: return setPlatformConfig(req, res, next);
    case settings.EXTERNAL_LDAP_KEY: return setExternalLdapConfig(req, res, next);
    case settings.UNSTABLE_APPS_KEY: return setUnstableAppsConfig(req, res, next);
    case settings.REGISTRY_CONFIG_KEY: return setRegistryConfig(req, res, next);
    case settings.SYSINFO_CONFIG_KEY: return setSysinfoConfig(req, res, next);

    case settings.APP_AUTOUPDATE_PATTERN_KEY: return setAppAutoupdatePattern(req, res, next);
    case settings.BOX_AUTOUPDATE_PATTERN_KEY: return setBoxAutoupdatePattern(req, res, next);
    case settings.TIME_ZONE_KEY: return setTimeZone(req, res, next);

    default: return next(new HttpError(404, 'No such setting'));
    }
}
