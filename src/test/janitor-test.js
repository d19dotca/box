/* jslint node:true */
/* global it:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var async = require('async'),
    BoxError = require('../boxerror.js'),
    database = require('../database'),
    expect = require('expect.js'),
    hat = require('../hat.js'),
    janitor = require('../janitor.js'),
    tokendb = require('../tokendb.js');

describe('janitor', function () {
    var TOKEN_0 = {
        id: 'tid-0',
        accessToken: hat(8 * 32),
        identifier: '0',
        clientId: 'clientid-0',
        expires: Date.now() + 60 * 60 * 1000,
        scope: 'settings',
        name: 'clientid0'
    };
    var TOKEN_1 = {
        id: 'tid-1',
        accessToken: hat(8 * 32),
        identifier: '1',
        clientId: 'clientid-1',
        expires: Date.now() - 1000,
        scope: 'apps',
        name: 'clientid1'
    };

    before(function (done) {
        async.series([
            database.initialize,
            database._clear,
            tokendb.add.bind(null, TOKEN_0),
            tokendb.add.bind(null, TOKEN_1)
        ], done);
    });

    after(function (done) {
        async.series([
            database._clear,
            database.uninitialize
        ], done);
    });

    it('can cleanupTokens', function (done) {
        janitor.cleanupTokens(done);
    });

    it('did not remove the non-expired token', function (done) {
        tokendb.getByAccessToken(TOKEN_0.accessToken, function (error, result) {
            expect(error).to.be(null);
            expect(result).to.be.eql(TOKEN_0);
            done();
        });
    });

    it('did remove the non-expired token', function (done) {
        tokendb.getByAccessToken(TOKEN_1.accessToken, function (error, result) {
            expect(error).to.be.a(BoxError);
            expect(error.reason).to.be(BoxError.NOT_FOUND);
            expect(result).to.not.be.ok();
            done();
        });
    });
});

