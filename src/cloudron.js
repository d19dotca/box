'use strict';

exports = module.exports = {
    initialize: initialize,
    uninitialize: uninitialize,
    getConfig: getConfig,
    getLogs: getLogs,

    reboot: reboot,
    isRebootRequired: isRebootRequired,

    onActivated: onActivated,

    prepareDashboardDomain: prepareDashboardDomain,
    setDashboardDomain: setDashboardDomain,
    setDashboardAndMailDomain: setDashboardAndMailDomain,
    renewCerts: renewCerts,

    setupDashboard: setupDashboard,

    runSystemChecks: runSystemChecks,
};

var addons = require('./addons.js'),
    apps = require('./apps.js'),
    appstore = require('./appstore.js'),
    assert = require('assert'),
    async = require('async'),
    auditSource = require('./auditsource.js'),
    backups = require('./backups.js'),
    BoxError = require('./boxerror.js'),
    constants = require('./constants.js'),
    cron = require('./cron.js'),
    debug = require('debug')('box:cloudron'),
    domains = require('./domains.js'),
    eventlog = require('./eventlog.js'),
    fs = require('fs'),
    mail = require('./mail.js'),
    notifications = require('./notifications.js'),
    path = require('path'),
    paths = require('./paths.js'),
    platform = require('./platform.js'),
    reverseProxy = require('./reverseproxy.js'),
    safe = require('safetydance'),
    settings = require('./settings.js'),
    shell = require('./shell.js'),
    spawn = require('child_process').spawn,
    split = require('split'),
    tasks = require('./tasks.js'),
    users = require('./users.js');

var REBOOT_CMD = path.join(__dirname, 'scripts/reboot.sh');

const NOOP_CALLBACK = function (error) { if (error) debug(error); };

function initialize(callback) {
    assert.strictEqual(typeof callback, 'function');

    runStartupTasks();

    notifyUpdate(callback);
}

function uninitialize(callback) {
    assert.strictEqual(typeof callback, 'function');

    async.series([
        cron.stopJobs,
        platform.stop
    ], callback);
}

function onActivated(callback) {
    assert.strictEqual(typeof callback, 'function');

    // Starting the platform after a user is available means:
    // 1. mail bounces can now be sent to the cloudron owner
    // 2. the restore code path can run without sudo (since mail/ is non-root)
    async.series([
        platform.start,
        cron.startJobs
    ], callback);
}

function notifyUpdate(callback) {
    assert.strictEqual(typeof callback, 'function');

    const version = safe.fs.readFileSync(paths.VERSION_FILE, 'utf8');
    if (version === constants.VERSION) return callback();

    eventlog.add(eventlog.ACTION_UPDATE_FINISH, auditSource.CRON, { errorMessage: '', oldVersion: version || 'dev', newVersion: constants.VERSION }, function (error) {
        if (error) return callback(error);

        tasks.setCompletedByType(tasks.TASK_UPDATE, { error: null }, function (error) {
            if (error && error.reason !== BoxError.NOT_FOUND) return callback(error); // when hotfixing, task may not exist

            safe.fs.writeFileSync(paths.VERSION_FILE, constants.VERSION, 'utf8');

            callback();
        });
    });
}

// each of these tasks can fail. we will add some routes to fix/re-run them
function runStartupTasks() {
    // configure nginx to be reachable by IP
    reverseProxy.writeDefaultConfig(NOOP_CALLBACK);

    // this configures collectd to collect backup storage metrics if filesystem is used. This is also triggerd when the settings change with the rest api
    settings.getBackupConfig(function (error, backupConfig) {
        if (error) return console.error('Failed to read backup config.', error);
        backups.configureCollectd(backupConfig, NOOP_CALLBACK);
    });

    // always generate webadmin config since we have no versioning mechanism for the ejs
    if (settings.adminDomain()) reverseProxy.writeAdminConfig(settings.adminDomain(), NOOP_CALLBACK);

    // check activation state and start the platform
    users.isActivated(function (error, activated) {
        if (error) return debug(error);
        if (!activated) return debug('initialize: not activated yet'); // not activated

        onActivated(NOOP_CALLBACK);
    });
}

function getConfig(callback) {
    assert.strictEqual(typeof callback, 'function');

    settings.getAll(function (error, allSettings) {
        if (error) return callback(error);

        // be picky about what we send out here since this is sent for 'normal' users as well
        callback(null, {
            apiServerOrigin: settings.apiServerOrigin(),
            webServerOrigin: settings.webServerOrigin(),
            adminDomain: settings.adminDomain(),
            adminFqdn: settings.adminFqdn(),
            mailFqdn: settings.mailFqdn(),
            version: constants.VERSION,
            isDemo: settings.isDemo(),
            cloudronName: allSettings[settings.CLOUDRON_NAME_KEY],
            footer: allSettings[settings.FOOTER_KEY] || constants.FOOTER,
            features: appstore.getFeatures()
        });
    });
}

function reboot(callback) {
    notifications.alert(notifications.ALERT_REBOOT, 'Reboot Required', '', function (error) {
        if (error) console.error('Failed to clear reboot notification.', error);

        shell.sudo('reboot', [ REBOOT_CMD ], {}, callback);
    });
}

function isRebootRequired(callback) {
    assert.strictEqual(typeof callback, 'function');

    // https://serverfault.com/questions/92932/how-does-ubuntu-keep-track-of-the-system-restart-required-flag-in-motd
    callback(null, fs.existsSync('/var/run/reboot-required'));
}

// called from cron.js
function runSystemChecks(callback) {
    assert.strictEqual(typeof callback, 'function');

    async.parallel([
        checkBackupConfiguration,
        checkMailStatus,
        checkRebootRequired
    ], callback);
}

function checkBackupConfiguration(callback) {
    assert.strictEqual(typeof callback, 'function');

    debug('checking backup configuration');

    backups.checkConfiguration(function (error, message) {
        if (error) return callback(error);

        notifications.alert(notifications.ALERT_BACKUP_CONFIG, 'Backup configuration is unsafe', message, callback);
    });
}

function checkMailStatus(callback) {
    assert.strictEqual(typeof callback, 'function');

    debug('checking mail status');

    mail.checkConfiguration(function (error, message) {
        if (error) return callback(error);

        notifications.alert(notifications.ALERT_MAIL_STATUS, 'Email is not configured properly', message, callback);
    });
}

function checkRebootRequired(callback) {
    assert.strictEqual(typeof callback, 'function');

    debug('checking if reboot required');

    isRebootRequired(function (error, rebootRequired) {
        if (error) return callback(error);

        notifications.alert(notifications.ALERT_REBOOT, 'Reboot Required', rebootRequired ? 'To finish ubuntu security updates, a reboot is necessary.' : '', callback);
    });
}

function getLogs(unit, options, callback) {
    assert.strictEqual(typeof unit, 'string');
    assert(options && typeof options === 'object');
    assert.strictEqual(typeof callback, 'function');

    assert.strictEqual(typeof options.lines, 'number');
    assert.strictEqual(typeof options.format, 'string');
    assert.strictEqual(typeof options.follow, 'boolean');

    var lines = options.lines === -1 ? '+1' : options.lines,
        format = options.format || 'json',
        follow = options.follow;

    debug('Getting logs for %s as %s', unit, format);

    let args = [ '--lines=' + lines ];
    if (follow) args.push('--follow');

    // need to handle box.log without subdir
    if (unit === 'box') args.push(path.join(paths.LOG_DIR, 'box.log'));
    else if (unit.startsWith('crash-')) args.push(path.join(paths.CRASH_LOG_DIR, unit.slice(6) + '.log'));
    else return callback(new BoxError(BoxError.BAD_FIELD, 'No such unit', { field: 'unit' }));

    var cp = spawn('/usr/bin/tail', args);

    var transformStream = split(function mapper(line) {
        if (format !== 'json') return line + '\n';

        var data = line.split(' '); // logs are <ISOtimestamp> <msg>
        var timestamp = (new Date(data[0])).getTime();
        if (isNaN(timestamp)) timestamp = 0;

        return JSON.stringify({
            realtimeTimestamp: timestamp * 1000,
            message: line.slice(data[0].length+1),
            source: unit
        }) + '\n';
    });

    transformStream.close = cp.kill.bind(cp, 'SIGKILL'); // closing stream kills the child process

    cp.stdout.pipe(transformStream);

    return callback(null, transformStream);
}

function prepareDashboardDomain(domain, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    debug(`prepareDashboardDomain: ${domain}`);

    if (settings.isDemo()) return callback(new BoxError(BoxError.CONFLICT, 'Not allowed in demo mode'));

    domains.get(domain, function (error, domainObject) {
        if (error) return callback(error);

        const fqdn = domains.fqdn(constants.ADMIN_LOCATION, domainObject);

        apps.getAll(function (error, result) {
            if (error) return callback(error);

            const conflict = result.filter(app => app.fqdn === fqdn);
            if (conflict.length) return callback(new BoxError(BoxError.BAD_STATE, 'Dashboard location conflicts with an existing app'));

            tasks.add(tasks.TASK_PREPARE_DASHBOARD_DOMAIN, [ domain, auditSource ], function (error, taskId) {
                if (error) return callback(error);

                tasks.startTask(taskId, {}, NOOP_CALLBACK);

                callback(null, taskId);
            });
        });
    });
}

// call this only pre activation since it won't start mail server
function setDashboardDomain(domain, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    debug(`setDashboardDomain: ${domain}`);

    domains.get(domain, function (error, domainObject) {
        if (error) return callback(error);

        reverseProxy.writeAdminConfig(domain, function (error) {
            if (error) return callback(error);

            const fqdn = domains.fqdn(constants.ADMIN_LOCATION, domainObject);

            settings.setAdmin(domain, fqdn, function (error) {
                if (error) return callback(error);

                eventlog.add(eventlog.ACTION_DASHBOARD_DOMAIN_UPDATE, auditSource, { domain: domain, fqdn: fqdn });

                callback(null);
            });
        });
    });
}

// call this only post activation because it will restart mail server
function setDashboardAndMailDomain(domain, auditSource, callback) {
    assert.strictEqual(typeof domain, 'string');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    debug(`setDashboardAndMailDomain: ${domain}`);

    if (settings.isDemo()) return callback(new BoxError(BoxError.CONFLICT, 'Not allowed in demo mode'));

    setDashboardDomain(domain, auditSource, function (error) {
        if (error) return callback(error);

        mail.onMailFqdnChanged(NOOP_CALLBACK); // this will update dns and re-configure mail server
        addons.restartService('turn', NOOP_CALLBACK); // to update the realm variable

        callback(null);
    });
}

function setupDashboard(auditSource, progressCallback, callback) {
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof progressCallback, 'function');
    assert.strictEqual(typeof callback, 'function');

    async.series([
        domains.prepareDashboardDomain.bind(null, settings.adminDomain(), auditSource, progressCallback),
        setDashboardDomain.bind(null, settings.adminDomain(), auditSource)
    ], callback);
}

function renewCerts(options, auditSource, callback) {
    assert.strictEqual(typeof options, 'object');
    assert.strictEqual(typeof auditSource, 'object');
    assert.strictEqual(typeof callback, 'function');

    tasks.add(tasks.TASK_RENEW_CERTS, [ options, auditSource ], function (error, taskId) {
        if (error) return callback(error);

        tasks.startTask(taskId, {}, NOOP_CALLBACK);

        callback(null, taskId);
    });
}
