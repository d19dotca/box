'use strict';

exports.up = function(db, callback) {
    db.runSql('ALTER TABLE backups ADD COLUMN encryptionVersion INTEGER', function (error) {
        if (error) return callback(error);

        db.all('SELECT value FROM settings WHERE name="backup_config"', function (error, results) {
            if (error || results.length === 0) return callback(error);

            var backupConfig = JSON.parse(results[0].value);
            if (!backupConfig.encryption) return callback(null);

            // mark old encrypted backups as v1
            db.runSql('UPDATE backups SET encryptionVersion=1', callback);
        });
    });
};

exports.down = function(db, callback) {
    db.runSql('ALTER TABLE backups DROP COLUMN encryptionVersion', function (error) {
        if (error) console.error(error);
        callback(error);
    });
};
