'use strict';

exports = module.exports = {
    proxy
};

var addons = require('../addons.js'),
    assert = require('assert'),
    BoxError = require('../boxerror.js'),
    middleware = require('../middleware/index.js'),
    HttpError = require('connect-lastmile').HttpError,
    url = require('url');

function proxy(req, res, next) {
    assert.strictEqual(typeof req.params.pathname, 'string');

    let parsedUrl = url.parse(req.url, true /* parseQueryString */);

    // do not proxy protected values
    delete parsedUrl.query['access_token'];
    delete req.headers['authorization'];
    delete req.headers['cookies'];

    addons.getContainerDetails('mail', 'CLOUDRON_MAIL_TOKEN', function (error, addonDetails) {
        if (error) return next(BoxError.toHttpError(error));

        parsedUrl.query['access_token'] = addonDetails.token;
        req.url = url.format({ pathname: req.params.pathname, query: parsedUrl.query });

        const proxyOptions = url.parse(`https://${addonDetails.ip}:3000`);
        proxyOptions.rejectUnauthorized = false;
        const mailserverProxy = middleware.proxy(proxyOptions);

        mailserverProxy(req, res, function (error) {
            if (!error) return next();

            if (error.code === 'ECONNREFUSED') return next(new HttpError(424, 'Unable to connect to mail server'));
            if (error.code === 'ECONNRESET') return next(new HttpError(424, 'Unable to query mail server'));

            next(new HttpError(500, error));
        });
    });
}
