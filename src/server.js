'use strict';

exports = module.exports = {
    start: start,
    stop: stop
};

let assert = require('assert'),
    async = require('async'),
    cloudron = require('./cloudron.js'),
    constants = require('./constants.js'),
    database = require('./database.js'),
    eventlog = require('./eventlog.js'),
    express = require('express'),
    http = require('http'),
    middleware = require('./middleware'),
    routes = require('./routes/index.js'),
    settings = require('./settings.js'),
    users = require('./users.js'),
    ws = require('ws');

var gHttpServer = null;

function initializeExpressSync() {
    var app = express();
    var httpServer = http.createServer(app);

    const wsServer = new ws.Server({ noServer: true }); // in noServer mode, we have to handle 'upgrade' and call handleUpgrade

    var QUERY_LIMIT = '1mb', // max size for json and urlencoded queries (see also client_max_body_size in nginx)
        FIELD_LIMIT = 2 * 1024 * 1024; // max fields that can appear in multipart

    var REQUEST_TIMEOUT = 20000; // timeout for all requests (see also setTimeout on the httpServer)

    var json = middleware.json({ strict: true, limit: QUERY_LIMIT }), // application/json
        urlencoded = middleware.urlencoded({ extended: false, limit: QUERY_LIMIT }); // application/x-www-form-urlencoded

    app.set('json spaces', 2); // pretty json

    // for rate limiting
    app.enable('trust proxy');

    if (process.env.BOX_ENV !== 'test') {
        app.use(middleware.morgan(function (tokens, req, res) {
            return [
                'Box',
                tokens.method(req, res),
                tokens.url(req, res).replace(/(access_token=)[^&]+/, '$1' + '<redacted>'),
                tokens.status(req, res),
                res.errorBody ? res.errorBody.status : '',  // attached by connect-lastmile. can be missing when router errors like 404
                res.errorBody ? res.errorBody.message : '', // attached by connect-lastmile. can be missing when router errors like 404
                tokens['response-time'](req, res), 'ms', '-',
                tokens.res(req, res, 'content-length')
            ].join(' ');
        }, {
            immediate: false,
            // only log failed requests by default
            skip: function (req, res) { return res.statusCode < 400; }
        }));
    }

    var router = new express.Router();
    router.del = router.delete; // amend router.del for readability further on

    app
        // the timeout middleware will respond with a 503. the request itself cannot be 'aborted' and will continue
        // search for req.clearTimeout in route handlers to see places where this timeout is reset
        .use(middleware.timeout(REQUEST_TIMEOUT, { respond: true }))
        .use(json)
        .use(urlencoded)
        .use(middleware.cors({ origins: [ '*' ], allowCredentials: false }))
        .use(router)
        .use(middleware.lastMile());

    // NOTE: routes that use multi-part have to be whitelisted in the reverse proxy
    var FILE_SIZE_LIMIT = '256mb', // max file size that can be uploaded (see also client_max_body_size in nginx)
        FILE_TIMEOUT = 60 * 1000; // increased timeout for file uploads (1 min)

    var multipart = middleware.multipart({ maxFieldsSize: FIELD_LIMIT, limit: FILE_SIZE_LIMIT, timeout: FILE_TIMEOUT });

    // to keep routes code short
    const password = routes.accesscontrol.passwordAuth;
    const token = routes.accesscontrol.tokenAuth;
    const authorizeOwner = routes.accesscontrol.authorize(users.ROLE_OWNER);
    const authorizeAdmin = routes.accesscontrol.authorize(users.ROLE_ADMIN);
    const authorizeUserManager = routes.accesscontrol.authorize(users.ROLE_USER_MANAGER);

    // public routes
    router.post('/api/v1/cloudron/setup', routes.provision.providerTokenAuth, routes.provision.setup);    // only available until no-domain
    router.post('/api/v1/cloudron/restore', routes.provision.restore);    // only available until activated
    router.post('/api/v1/cloudron/activate', routes.provision.activate);
    router.get ('/api/v1/cloudron/status', routes.provision.getStatus);

    router.get ('/api/v1/cloudron/avatar', routes.branding.getCloudronAvatar); // this is a public alias for /api/v1/branding/cloudron_avatar

    // login/logout routes
    router.post('/api/v1/cloudron/login', password, routes.cloudron.login);
    router.get ('/api/v1/cloudron/logout', routes.cloudron.logout); // this will invalidate the token if any and redirect to /login.html always
    router.post('/api/v1/cloudron/password_reset_request', routes.cloudron.passwordResetRequest);
    router.post('/api/v1/cloudron/password_reset', routes.cloudron.passwordReset);
    router.post('/api/v1/cloudron/setup_account', routes.cloudron.setupAccount);

    // developer routes
    router.post('/api/v1/developer/login', password, routes.cloudron.login); // DEPRECATED we should use the regular /api/v1/cloudron/login

    // cloudron routes
    router.get ('/api/v1/cloudron/update', token, authorizeAdmin, routes.cloudron.getUpdateInfo);
    router.post('/api/v1/cloudron/update', token, authorizeAdmin, routes.cloudron.update);
    router.post('/api/v1/cloudron/prepare_dashboard_domain', token, authorizeAdmin, routes.cloudron.prepareDashboardDomain);
    router.post('/api/v1/cloudron/set_dashboard_domain', token, authorizeAdmin, routes.cloudron.setDashboardAndMailDomain);
    router.post('/api/v1/cloudron/renew_certs', token, authorizeAdmin, routes.cloudron.renewCerts);
    router.post('/api/v1/cloudron/check_for_updates', token, authorizeAdmin, routes.cloudron.checkForUpdates);
    router.get ('/api/v1/cloudron/reboot', token, authorizeAdmin, routes.cloudron.isRebootRequired);
    router.post('/api/v1/cloudron/reboot', token, authorizeAdmin, routes.cloudron.reboot);
    router.get ('/api/v1/cloudron/graphs', token, authorizeAdmin, routes.graphs.getGraphs);
    router.get ('/api/v1/cloudron/disks', token, authorizeAdmin, routes.cloudron.getDisks);
    router.get ('/api/v1/cloudron/memory', token, authorizeAdmin, routes.cloudron.getMemory);
    router.get ('/api/v1/cloudron/logs/:unit', token, authorizeAdmin, routes.cloudron.getLogs);
    router.get ('/api/v1/cloudron/logstream/:unit', token, authorizeAdmin, routes.cloudron.getLogStream);
    router.get ('/api/v1/cloudron/eventlog', token, authorizeAdmin, routes.eventlog.list);
    router.get ('/api/v1/cloudron/eventlog/:eventId', token, authorizeAdmin, routes.eventlog.get);
    router.post('/api/v1/cloudron/sync_external_ldap', token, authorizeAdmin, routes.cloudron.syncExternalLdap);
    router.get ('/api/v1/cloudron/server_ip', token, authorizeAdmin, routes.cloudron.getServerIp);

    // tasks
    router.get ('/api/v1/tasks', token, authorizeAdmin, routes.tasks.list);
    router.get ('/api/v1/tasks/:taskId', token, authorizeAdmin, routes.tasks.get);
    router.get ('/api/v1/tasks/:taskId/logs', token, authorizeAdmin, routes.tasks.getLogs);
    router.get ('/api/v1/tasks/:taskId/logstream', token, authorizeAdmin, routes.tasks.getLogStream);
    router.post('/api/v1/tasks/:taskId/stop', token, authorizeAdmin, routes.tasks.stopTask);

    // notifications
    router.get ('/api/v1/notifications', token, routes.notifications.verifyOwnership, routes.notifications.list);
    router.get ('/api/v1/notifications/:notificationId', token, routes.notifications.verifyOwnership, routes.notifications.get);
    router.post('/api/v1/notifications/:notificationId', token, routes.notifications.verifyOwnership, routes.notifications.ack);

    // backups
    router.get ('/api/v1/backups', token, authorizeAdmin, routes.backups.list);
    router.post('/api/v1/backups/create', token, authorizeAdmin, routes.backups.startBackup);
    router.post('/api/v1/backups/cleanup', token, authorizeAdmin, routes.backups.cleanup);

    // config route (for dashboard)
    router.get ('/api/v1/config', token, routes.cloudron.getConfig);

    // working off the user behind the provided token
    router.get ('/api/v1/profile', token, routes.profile.get);
    router.post('/api/v1/profile', token, routes.profile.update);
    router.get ('/api/v1/profile/avatar/:identifier', routes.profile.getAvatar); // this is not scoped so it can used directly in img tag
    router.post('/api/v1/profile/avatar', token, multipart, routes.profile.setAvatar);
    router.del ('/api/v1/profile/avatar', token, routes.profile.clearAvatar);
    router.post('/api/v1/profile/password', token, routes.users.verifyPassword, routes.profile.changePassword);
    router.post('/api/v1/profile/twofactorauthentication', token, routes.profile.setTwoFactorAuthenticationSecret);
    router.post('/api/v1/profile/twofactorauthentication/enable', token, routes.profile.enableTwoFactorAuthentication);
    router.post('/api/v1/profile/twofactorauthentication/disable', token, routes.users.verifyPassword, routes.profile.disableTwoFactorAuthentication);

    router.get ('/api/v1/app_passwords', token, routes.appPasswords.list);
    router.post('/api/v1/app_passwords', token, routes.appPasswords.add);
    router.get ('/api/v1/app_passwords/:id', token, routes.appPasswords.get);
    router.del ('/api/v1/app_passwords/:id', token, routes.appPasswords.del);

    // access tokens
    router.get ('/api/v1/tokens', token, routes.tokens.getAll);
    router.post('/api/v1/tokens', token, routes.tokens.add);
    router.get ('/api/v1/tokens/:id', token, routes.tokens.verifyOwnership, routes.tokens.get);
    router.del ('/api/v1/tokens/:id', token, routes.tokens.verifyOwnership, routes.tokens.del);

    // user routes
    router.get ('/api/v1/users', token, authorizeUserManager, routes.users.list);
    router.post('/api/v1/users', token, authorizeUserManager, routes.users.create);
    router.get ('/api/v1/users/:userId', token, authorizeUserManager, routes.users.load, routes.users.get); // this is manage scope because it returns non-restricted fields
    router.del ('/api/v1/users/:userId', token, authorizeUserManager, routes.users.load, routes.users.remove);
    router.post('/api/v1/users/:userId', token, authorizeUserManager, routes.users.load, routes.users.update);
    router.post('/api/v1/users/:userId/password', token, authorizeUserManager, routes.users.load, routes.users.changePassword);
    router.put ('/api/v1/users/:userId/groups', token, authorizeUserManager, routes.users.load, routes.users.setGroups);
    router.post('/api/v1/users/:userId/send_invite', token, authorizeUserManager, routes.users.load, routes.users.sendInvite);
    router.post('/api/v1/users/:userId/create_invite', token, authorizeUserManager, routes.users.load, routes.users.createInvite);

    // Group management
    router.get ('/api/v1/groups', token, authorizeUserManager, routes.groups.list);
    router.post('/api/v1/groups', token, authorizeUserManager, routes.groups.create);
    router.get ('/api/v1/groups/:groupId', token, authorizeUserManager, routes.groups.get);
    router.put ('/api/v1/groups/:groupId/members', token, authorizeUserManager, routes.groups.updateMembers);
    router.post('/api/v1/groups/:groupId', token, authorizeUserManager, routes.groups.update);
    router.del ('/api/v1/groups/:groupId', token, authorizeUserManager, routes.groups.remove);

    // appstore and subscription routes
    router.post('/api/v1/appstore/register_cloudron', token, authorizeAdmin, routes.appstore.registerCloudron);
    router.post('/api/v1/appstore/user_token', token, authorizeAdmin, routes.appstore.createUserToken);
    router.get ('/api/v1/appstore/subscription', token, authorizeAdmin, routes.appstore.getSubscription);
    router.get ('/api/v1/appstore/apps', token, authorizeAdmin, routes.appstore.getApps);
    router.get ('/api/v1/appstore/apps/:appstoreId', token, authorizeAdmin, routes.appstore.getApp);
    router.get ('/api/v1/appstore/apps/:appstoreId/versions/:versionId', token, authorizeAdmin, routes.appstore.getAppVersion);

    // app routes
    router.get ('/api/v1/apps',          token, routes.apps.getApps);
    router.get ('/api/v1/apps/:id',      token, authorizeAdmin, routes.apps.load, routes.apps.getApp);
    router.get ('/api/v1/apps/:id/icon', token, routes.apps.load, routes.apps.getAppIcon);

    router.post('/api/v1/apps/install',       token, authorizeAdmin, routes.apps.install);
    router.post('/api/v1/apps/:id/uninstall', token, authorizeAdmin, routes.apps.load, routes.apps.uninstall);

    router.post('/api/v1/apps/:id/configure/access_restriction', token, authorizeAdmin, routes.apps.load, routes.apps.setAccessRestriction);
    router.post('/api/v1/apps/:id/configure/label', token, authorizeAdmin, routes.apps.load, routes.apps.setLabel);
    router.post('/api/v1/apps/:id/configure/tags', token, authorizeAdmin, routes.apps.load, routes.apps.setTags);
    router.post('/api/v1/apps/:id/configure/icon', token, authorizeAdmin, routes.apps.load, routes.apps.setIcon);
    router.post('/api/v1/apps/:id/configure/memory_limit', token, authorizeAdmin, routes.apps.load, routes.apps.setMemoryLimit);
    router.post('/api/v1/apps/:id/configure/cpu_shares', token, authorizeAdmin, routes.apps.load, routes.apps.setCpuShares);
    router.post('/api/v1/apps/:id/configure/automatic_backup', token, authorizeAdmin, routes.apps.load, routes.apps.setAutomaticBackup);
    router.post('/api/v1/apps/:id/configure/automatic_update', token, authorizeAdmin, routes.apps.load, routes.apps.setAutomaticUpdate);
    router.post('/api/v1/apps/:id/configure/reverse_proxy', token, authorizeAdmin, routes.apps.load, routes.apps.setReverseProxyConfig);
    router.post('/api/v1/apps/:id/configure/cert', token, authorizeAdmin, routes.apps.load, routes.apps.setCertificate);
    router.post('/api/v1/apps/:id/configure/debug_mode', token, authorizeAdmin, routes.apps.load, routes.apps.setDebugMode);
    router.post('/api/v1/apps/:id/configure/mailbox', token, authorizeAdmin, routes.apps.load, routes.apps.setMailbox);
    router.post('/api/v1/apps/:id/configure/env', token, authorizeAdmin, routes.apps.load, routes.apps.setEnvironment);
    router.post('/api/v1/apps/:id/configure/data_dir', token, authorizeAdmin, routes.apps.load, routes.apps.setDataDir);
    router.post('/api/v1/apps/:id/configure/location', token, authorizeAdmin, routes.apps.load, routes.apps.setLocation);
    router.post('/api/v1/apps/:id/configure/binds', token, authorizeAdmin, routes.apps.load, routes.apps.setBinds);

    router.post('/api/v1/apps/:id/repair',    token, authorizeAdmin, routes.apps.load, routes.apps.repair);
    router.post('/api/v1/apps/:id/update',    token, authorizeAdmin, routes.apps.load, routes.apps.update);
    router.post('/api/v1/apps/:id/restore',   token, authorizeAdmin, routes.apps.load, routes.apps.restore);
    router.post('/api/v1/apps/:id/import',    token, authorizeAdmin, routes.apps.load, routes.apps.importApp);
    router.post('/api/v1/apps/:id/backup',    token, authorizeAdmin, routes.apps.load, routes.apps.backup);
    router.get ('/api/v1/apps/:id/backups',   token, authorizeAdmin, routes.apps.load, routes.apps.listBackups);
    router.post('/api/v1/apps/:id/start',     token, authorizeAdmin, routes.apps.load, routes.apps.start);
    router.post('/api/v1/apps/:id/stop',      token, authorizeAdmin, routes.apps.load, routes.apps.stop);
    router.post('/api/v1/apps/:id/restart',   token, authorizeAdmin, routes.apps.load, routes.apps.restart);
    router.get ('/api/v1/apps/:id/logstream', token, authorizeAdmin, routes.apps.load, routes.apps.getLogStream);
    router.get ('/api/v1/apps/:id/logs',      token, authorizeAdmin, routes.apps.load, routes.apps.getLogs);
    router.get ('/api/v1/apps/:id/exec',      token, authorizeAdmin, routes.apps.load, routes.apps.exec);
    // websocket cannot do bearer authentication
    router.get ('/api/v1/apps/:id/execws',    routes.accesscontrol.websocketAuth.bind(null, users.ROLE_ADMIN), routes.apps.load, routes.apps.execWebSocket);
    router.post('/api/v1/apps/:id/clone',     token, authorizeAdmin, routes.apps.load, routes.apps.clone);
    router.get ('/api/v1/apps/:id/download',  token, authorizeAdmin, routes.apps.load, routes.apps.downloadFile);
    router.post('/api/v1/apps/:id/upload',    token, authorizeAdmin, multipart, routes.apps.load, routes.apps.uploadFile);

    router.get ('/api/v1/branding/:setting', token, authorizeOwner, routes.branding.get);
    router.post('/api/v1/branding/:setting', token, authorizeOwner,  (req, res, next) => {
        return req.params.setting === 'cloudron_avatar' ? multipart(req, res, next) : next();
    }, routes.branding.set);

    // settings routes (these are for the settings tab - avatar & name have public routes for normal users. see above)
    router.get ('/api/v1/settings/:setting', token, authorizeAdmin, routes.settings.get);
    router.post('/api/v1/settings/backup_config', token, authorizeOwner, routes.settings.setBackupConfig);
    router.post('/api/v1/settings/:setting', token, authorizeAdmin, routes.settings.set);

    // email routes
    router.get('/api/v1/mailserver/:pathname', token, (req, res, next) => {
        // some routes are more special than others
        if (req.params.pathname === 'eventlog' || req.params.pathname === 'clear_eventlog') {
            return authorizeOwner(req, res, next);
        }
        authorizeAdmin(req, res, next);
    }, routes.mailserver.proxy);

    router.get ('/api/v1/mail/:domain',       token, authorizeAdmin, routes.mail.getDomain);
    router.get ('/api/v1/mail/:domain/status',       token, authorizeAdmin, routes.mail.getStatus);
    router.post('/api/v1/mail/:domain/mail_from_validation', token, authorizeAdmin, routes.mail.setMailFromValidation);
    router.post('/api/v1/mail/:domain/catch_all',  token, authorizeAdmin, routes.mail.setCatchAllAddress);
    router.post('/api/v1/mail/:domain/relay',         token, authorizeAdmin, routes.mail.setMailRelay);
    router.post('/api/v1/mail/:domain/enable',        token, authorizeAdmin, routes.mail.setMailEnabled);
    router.post('/api/v1/mail/:domain/dns',        token, authorizeAdmin, routes.mail.setDnsRecords);
    router.post('/api/v1/mail/:domain/send_test_mail',  token, authorizeAdmin, routes.mail.sendTestMail);
    router.get ('/api/v1/mail/:domain/mailboxes',  token, authorizeAdmin, routes.mail.listMailboxes);
    router.get ('/api/v1/mail/:domain/mailboxes/:name',  token, authorizeAdmin, routes.mail.getMailbox);
    router.post('/api/v1/mail/:domain/mailboxes',  token, authorizeAdmin, routes.mail.addMailbox);
    router.post('/api/v1/mail/:domain/mailboxes/:name',  token, authorizeAdmin, routes.mail.updateMailbox);
    router.del ('/api/v1/mail/:domain/mailboxes/:name',  token, authorizeAdmin, routes.mail.removeMailbox);
    router.get ('/api/v1/mail/:domain/mailboxes/:name/aliases', token, authorizeAdmin, routes.mail.getAliases);
    router.put ('/api/v1/mail/:domain/mailboxes/:name/aliases', token, authorizeAdmin, routes.mail.setAliases);

    router.get ('/api/v1/mail/:domain/lists', token, authorizeAdmin, routes.mail.getLists);
    router.post('/api/v1/mail/:domain/lists', token, authorizeAdmin, routes.mail.addList);
    router.get ('/api/v1/mail/:domain/lists/:name', token, authorizeAdmin, routes.mail.getList);
    router.post('/api/v1/mail/:domain/lists/:name', token, authorizeAdmin, routes.mail.updateList);
    router.del ('/api/v1/mail/:domain/lists/:name', token, authorizeAdmin, routes.mail.removeList);

    // support
    router.post('/api/v1/support/ticket',         token, authorizeAdmin, routes.support.canCreateTicket, routes.support.createTicket);
    router.get ('/api/v1/support/remote_support', token, authorizeAdmin, routes.support.getRemoteSupport);
    router.post('/api/v1/support/remote_support', token, authorizeAdmin, routes.support.canEnableRemoteSupport, routes.support.enableRemoteSupport);

    // domain routes
    router.post('/api/v1/domains', token, authorizeAdmin, routes.domains.add);
    router.get ('/api/v1/domains', token, routes.domains.getAll);
    router.get ('/api/v1/domains/:domain', token, authorizeAdmin, routes.domains.get);  // this is manage scope because it returns non-restricted fields
    router.put ('/api/v1/domains/:domain', token, authorizeAdmin, routes.domains.update);
    router.del ('/api/v1/domains/:domain', token, authorizeAdmin, routes.domains.del);
    router.get ('/api/v1/domains/:domain/dns_check', token, authorizeAdmin, routes.domains.checkDnsRecords);

    // addon routes
    router.get ('/api/v1/services', token, authorizeAdmin, routes.services.getAll);
    router.get ('/api/v1/services/:service', token, authorizeAdmin, routes.services.get);
    router.post('/api/v1/services/:service', token, authorizeAdmin, routes.services.configure);
    router.get ('/api/v1/services/:service/logs', token, authorizeAdmin, routes.services.getLogs);
    router.get ('/api/v1/services/:service/logstream', token, authorizeAdmin, routes.services.getLogStream);
    router.post('/api/v1/services/:service/restart', token, authorizeAdmin, routes.services.restart);

    // disable server socket "idle" timeout. we use the timeout middleware to handle timeouts on a route level
    // we rely on nginx for timeouts on the TCP level (see client_header_timeout)
    httpServer.setTimeout(0);

    // upgrade handler
    httpServer.on('upgrade', function (req, socket, head) {
        // create a node response object for express
        var res = new http.ServerResponse({});
        res.assignSocket(socket);

        if (req.headers.upgrade === 'websocket') {
            res.handleUpgrade = function (callback) {
                wsServer.handleUpgrade(req, socket, head, callback);
            };
        } else {
            res.sendUpgradeHandshake = function () { // could extend express.response as well
                socket.write('HTTP/1.1 101 TCP Handshake\r\n' +
                             'Upgrade: tcp\r\n' +
                             'Connection: Upgrade\r\n' +
                             '\r\n');
            };
        }

        // route through express middleware. if we provide no callback, express will provide a 'finalhandler'
        // TODO: it's not clear if socket needs to be destroyed
        app(req, res);
    });

    return httpServer;
}

function start(callback) {
    assert.strictEqual(typeof callback, 'function');
    assert.strictEqual(gHttpServer, null, 'Server is already up and running.');

    gHttpServer = initializeExpressSync();

    async.series([
        database.initialize,
        settings.initCache, // pre-load very often used settings
        cloudron.initialize,
        gHttpServer.listen.bind(gHttpServer, constants.PORT, '127.0.0.1'),
        eventlog.add.bind(null, eventlog.ACTION_START, { userId: null, username: 'boot' }, { version: constants.VERSION })
    ], callback);
}

function stop(callback) {
    assert.strictEqual(typeof callback, 'function');

    if (!gHttpServer) return callback(null);

    async.series([
        cloudron.uninitialize,
        database.uninitialize,
        gHttpServer.close.bind(gHttpServer),
    ], function (error) {
        if (error) return callback(error);

        gHttpServer = null;

        callback(null);
    });
}
