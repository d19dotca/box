'use strict';

exports = module.exports = {
    getServerIp: getServerIp,
    testConfig: testConfig,

    hasIPv6: hasIPv6
};

var assert = require('assert'),
    fs = require('fs'),
    settings = require('./settings.js');

function api(provider) {
    assert.strictEqual(typeof provider, 'string');

    switch (provider) {
    case 'fixed': return require('./sysinfo/fixed.js');
    case 'network-interface': return require('./sysinfo/network-interface.js');
    default: return require('./sysinfo/generic.js');
    }
}

function getServerIp(callback) {
    assert.strictEqual(typeof callback, 'function');

    settings.getSysinfoConfig(function (error, config) {
        if (error) return callback(error);

        api(config.provider).getServerIp(config, callback);
    });
}

function hasIPv6() {
    const IPV6_PROC_FILE = '/proc/net/if_inet6';
    // on contabo, /proc/net/if_inet6 is an empty file. so just exists is not enough
    return fs.existsSync(IPV6_PROC_FILE) && fs.readFileSync(IPV6_PROC_FILE, 'utf8').trim().length !== 0;
}

function testConfig(config, callback) {
    assert.strictEqual(typeof config, 'object');
    assert.strictEqual(typeof callback, 'function');

    api(config.provider).testConfig(config, callback);
}
